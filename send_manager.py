import psycopg2, csv, time
import lib_email

lst = []
logins = ''
with open('bad_os.txt') as f:
    for line in f:
        logins += "'" + line.strip() + "',"
    
logins = logins.strip(',')
sql_txt = '''
    select
        distinct l.data->>'client_id' ,
         l.data->>'name',
         l.data->>'manager_id' ,
         l.data->>'manager_name',
         l.data->>'manager_email'
    from
        tbl.logins as l
    where
        convert_from(l.id,'UTF8') in 
    (''' + logins + ')'

#print(sql_txt,logins)

try:
    con = psycopg2.connect(
        database = 'db_site',
        user='db_dev',
        password='',
        host='web.local',
        port='5432'
    )

    cur = con.cursor()
    cur.execute(sql_txt)

    rows = cur.fetchall()
    for row in rows:
        lst.append(row)
    con.close()

except Exception as ex:
    print('main :' + ex)

def write_log(file_name):
    file = open(file_name, 'w',newline="", encoding="utf-8" ) # "utf-8" cp1251
    writer = csv.writer(file,delimiter = ';')
    return writer 
spisok_clients = write_log('spisok_clients.csv')

spisok_clients.writerow(['client_id', 'client_name', 'manager_name', 'manager_email'])	
for data in lst:
    spisok_clients.writerow([data[0], data[1], data[3], data[4]] )

#------------------------------------------------------------------------
manager = set([data[2] for data in lst])

for mng in manager:
    print(f"\nМенеджер - {mng}")
    clients = []
    for data in lst:
        if data[2] == str(mng) :
            print( data[0], data[1], data[2],  data[3], data[4] )
            if  data[4] != '' : 
                clients.append(data[0] +'  '+ data[1])
                name_manager=data[3]
                toaddr = data[4]
                
                
    lib_email.sender_mail_post(name_manager=name_manager, clients=clients , toaddr = [toaddr,'m.tov@aunt.ru'])
    time.sleep(4)
