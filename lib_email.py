import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart

def sender_mail_post(**params):
    try:
        msg_txt = 'Добрый день, ' +  params['name_manager'] + '!\n' \
        + 'Были найдены клиент(ы), которые используют, для работы с нашим сайтом заказов, устаревшее программное обеспечение, точнее ОС Windows XP. \n' \
        + 'Корпорация Майкрософт уже её не поддерживает с апреля 2014 года. \n' \
        + 'И больше не выпускает обновления для системы безопасности этой ОС.\n' \
        + 'Перейти на современную операционную систему КРАЙНЕ ВАЖНО для работы с нашим сайтом !!! \n' \
        + 'Необходимо убедить клиентов о переходе на "свежую" версию Windows . \n\n'\
        + 'Предоставляю список клиентов "для обзвона" :\n\n' \
        + '\n'.join(params['clients']) +' \n'\
        + '\n\n С уважением,\n \n Сотрудник ИТ отдела.\n  Иванов АА \n'\
        +'''\n
Office: +7 (812) 000-00-00
 \n ''' 

        msg = MIMEMultipart('alternative')
        sender = 'm.lov@aut.ru'
        recipients = params['toaddr']

        msg['Subject'] = 'Важная новость !' 
        msg['From'] = sender
        msg['To'] = ", ".join(recipients)
        msg.attach(MIMEText(msg_txt))
    
    except Exception as ex:
        print('bin : ' + ex)
        exit()

    try:
        with smtplib.SMTP('smtp.autont.ru') as s:
            s.set_debuglevel(0)
            s.sendmail(sender, recipients, msg.as_string())

    except smtplib.SMTPException as ex:
        print('bin : ' + ex)
