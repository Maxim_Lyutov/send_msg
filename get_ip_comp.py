#!/usr/bin/python3
import csv, requests

def write_log(file_name):
    file = open(file_name, 'w',newline="",encoding="utf8")
    writer = csv.writer(file,delimiter = ';')
    return writer

def read_csv(file_csv):
    res = {}
    with open(file_csv,"r" , newline="", encoding="utf8") as file:
        reader = csv.reader(file, delimiter = ';' )
        for row in reader:
            res.setdefault(row[0].strip(), row[1].strip())
    return res

ip = read_csv('in_bad_api.csv')
comp = read_csv('ip_comp.csv')

writer = write_log('out_bad_ip_login.csv')
writer2 = write_log('out_good_ip_login.csv')

for  login  in ip.keys():
    if comp.get(ip[login],None) != None :
            writer2.writerow([ip[login], login, comp[ip[login]]])
            print(f"{comp[ip[login]]} - {ip[login]} ")
    else :
        print(f"не найден - {ip[login]}" )
        try:
            res = requests.get('http://' + ip[login], verify=False, timeout=4)
        except requests.exceptions.RequestException as err:
            pass
        finally:
            writer.writerow([ip[login], login , 'bad'])
